<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
   protected $table = 'CONTACT';
    protected $primaryKey = 'IdContact';//['IdEntreprise','VisDate','IdContact']
    public $timestamps = false;


    public function Visite()
    {
      return $this->hasMany(Contact::class,"IdContact","IdContact");
    }

    public function Entreprise()
    {
      return $this->hasOne(Entreprise::class,"IdEntreprise","IdEntreprise");
    }
}