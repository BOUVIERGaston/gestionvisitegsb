<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    protected $table = 'ENTREPRISE';
    protected $primaryKey = 'IdEntreprise';//['IdEntreprise','VisDate','IdContact']
    public $timestamps = false;


    public function Visite()
    {
      return $this->hasMany(Entreprise::class,"IdEntreprise","IdEntreprise");
    }


    public function Secteur()
    {
      return $this->hasOne(Secteur::class,"IdSecteur","IdSecteur");
    }

    public function Contact()
    {
      return $this->hasMany(Entreprise::class,"IdEntreprise","IdEntreprise");
    }
}
