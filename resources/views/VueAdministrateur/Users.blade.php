@include('header',['title'=> 'Utilisateurs'])
@include('navbar',['title'=> "Visualisation des Utilisateurs"])


<body>

   <br>
   <div class="row text-center" id="search-form">
      <form method='POST' action=''>
                     
         @method('PUT')
         @csrf  
         <input type="text" class="form-control" name="search" id="search-input" placeholder="Rechercher par nom" 
         @if(isset($content_search)) value="{{$content_search}}"@endif>
         <button class="btn btn-outline-success" type="submit">Rechercher</button>
         <a href="/User/register" class="button_create_table"><button type="button" class="btn btn-success">Créer un utilisateur</button></a>
      </form>
      
   </div>
   <br>
   
   <div class="row justify-content-center">
      <div class="col-1"></div>
      <div class="col-10">
         <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
               <th scope="col">Nom</th>
               <th scope="col">Email</th>
               <th scope="col">Role</th>
               <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
               @foreach ($users as $user)
                  <tr>
                     <td>{{ $user->name }}</td>
                     <td>{{ $user->email }}</td>
                     <td>
                        <?php
                           $droit = DB::select('SELECT role_IdRole FROM role_user WHERE user_id = ?', [$user->id]);
                           $droit = count($droit);
                           if($droit == 1) echo "Consultant";
                           if($droit == 2) echo "Commercial";
                           if($droit == 3) echo "Administrateur";
                        ?>
                     </td>
                     <td>
                        <a href="/User/gestion/{{$user->id}}"><button type="button" class="btn btn-primary">Modifier</button></a>
                        <a href="/User/Suppression/{{$user->id}}"><button type="button" class="btn btn-danger">Supprimer</button></a>
                     </td>
                  <tr>
               @endforeach
            </tbody>
         </table>
      </div>
      <div class="col-1"></div>
   </div>
</body>
<footer>
   @if(isset($users))
      <ul class="pagination justify-content-center mb-4">
         {{$users->links("pagination::bootstrap-4")}}
      </ul>
   @endif
</footer>