@include('header',['title'=> 'Supression Visite'])
@include('navbar',['title'=> "Supression d'une visite"])
<?php
   use App\Models\Contact;
   $contact = Contact::find($visite->IdContact);
?>

<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Etes vous sûre de vouloir supprimer cette visite ?</h5>
               <div class="card-body">
                  Vous êtes sur le point de supprimer la visite suivante :<br>
                  - Entreprise : {{$visite->Entreprise->EntNom}}<br>
                  - Contact : {{$contact->ConNom." ".$contact->ConPrenom}}<br>
                  - Date : {{$visite->VisDate}}<br>
                  Attention, cette action est irréversible
                  <br><br>
                  <div class="text-center button-suppression">
                     <a href="/Visite/gestion/{{$visite->id}}"><button class="btn btn-danger">Retour</button></a>
                     <form method='POST' action=''>
                        @method('PUT')
                        @csrf 
                        <button type="submit" class="btn btn-success">Oui Supprimer</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>