<?php

namespace App\Http\Controllers;

use App\Models\Visite;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function init(){
        $visites = Visite::orderBy('VisDate', 'desc')->paginate(12);
        return view('EspaceUtilisateur',['visites'=>$visites]);
    }
}
