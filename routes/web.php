<?php

use App\Http\Controllers\EntrepriseController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\SecteurController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\VisiteController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\RegisteredUserController;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Role;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    redirect("login");
});

////////////////////////////////////////////////////////////////////////
//////////////////////////// COMMERCIAL ////////////////////////////////
////////////////////////////////////////////////////////////////////////

Route::middleware(['auth','role:commercial'])->group(function () {

    // Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    // Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    // Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    
    Route::get('/home',[MenuController::class,'init'])->name('menu.create');
    // Visite
    Route::get('/Visite/gestion/{id?}',[VisiteController::class,'show'])->where("id","[0-9]+");
    Route::put('/Visite/gestion/{id?}',[VisiteController::class,'update'])->where("id","[0-9]+");

    Route::get('/Visite/new',[VisiteController::class,'before_create']);
    Route::put('/Visite/new',[VisiteController::class,'create']);

    Route::get('/Visite/Suppression/{id?}',[VisiteController::class,'confirmDelete'])->where("id","[0-9]+");
    Route::put('/Visite/Suppression/{id?}',[VisiteController::class,'delete'])->where("id","[0-9]+");
    

    // Entreprise
    Route::get('/Entreprise/gestion/{id?}',[EntrepriseController::class,'show'])->where("id","[0-9]+");
    Route::put('/Entreprise/gestion/{id?}',[EntrepriseController::class,'update'])->where("id","[0-9]+");

    Route::get('/Entreprise/new',[EntrepriseController::class,'before_create']);
    Route::put('/Entreprise/new',[EntrepriseController::class,'create']);

    Route::get('/Entreprise/all',[EntrepriseController::class,'paginate']);
    Route::put('/Entreprise/all',[EntrepriseController::class,'paginate_search']);

    Route::get('/Entreprise/Suppression/{id?}',[EntrepriseController::class,'confirmDelete'])->where("id","[0-9]+");
    Route::put('/Entreprise/Suppression/{id?}',[EntrepriseController::class,'delete'])->where("id","[0-9]+");

    // Contact
    Route::get('/Contact/gestion/{id?}',[ContactController::class,'show'])->where("id","[0-9]+");
    Route::put('/Contact/gestion/{id?}',[ContactController::class,'update'])->where("id","[0-9]+");

    Route::get('/Contact/new',[ContactController::class,'before_create']);
    Route::put('/Contact/new',[ContactController::class,'create']);

    Route::get('/Contact/all',[ContactController::class,'paginate']);
    Route::put('/Contact/all',[ContactController::class,'paginate_search']);

    Route::get('/Contact/Suppression/{id?}',[ContactController::class,'confirmDelete'])->where("id","[0-9]+");
    Route::put('/Contact/Suppression/{id?}',[ContactController::class,'delete'])->where("id","[0-9]+");

    // Secteur
    Route::get('/Secteur/gestion/{id?}',[SecteurController::class,'infos'])->where("id","[0-9]+");
});

////////////////////////////////////////////////////////////////////////
////////////////////////// ADMINISTRATEUR //////////////////////////////
////////////////////////////////////////////////////////////////////////

Route::middleware(['auth','role:admin'])->group(function () {
    // User
    Route::get('/User/all',[UserController::class,'paginate']);
    Route::put('/User/all',[UserController::class,'paginate_search']);

    Route::get('/User/gestion/{id?}',[UserController::class,'show'])->where("id","[0-9]+");
    Route::put('/User/gestion/{id?}',[UserController::class,'update'])->where("id","[0-9]+");

    Route::get('/User/Suppression/{id?}',[UserController::class,'confirmDelete'])->where("id","[0-9]+");
    Route::put('/User/Suppression/{id?}',[UserController::class,'delete'])->where("id","[0-9]+");

    Route::get('User/register', [RegisteredUserController::class, 'create'])->name('register');
    Route::post('User/register', [RegisteredUserController::class, 'store']);

    // Secteur
    Route::get('/Secteur/gestion/{id?}',[SecteurController::class,'infos'])->where("id","[0-9]+");
    Route::put('/Secteur/gestion/{id?}',[SecteurController::class,'update'])->where("id","[0-9]+");
    
    Route::get('/Secteur/new',[SecteurController::class,'before_create']);
    Route::put('/Secteur/new',[SecteurController::class,'create']);

    Route::get('/Secteur/all',[SecteurController::class,'paginate']);
    Route::put('/Secteur/all',[SecteurController::class,'paginate_search']);

    Route::get('/Secteur/Suppression/{id?}',[SecteurController::class,'confirmDelete'])->where("id","[0-9]+");
    Route::put('/Secteur/Suppression/{id?}',[SecteurController::class,'delete'])->where("id","[0-9]+");
});



////////////////////////////////////////////////////////////////////////
//////////////////////////// CONSULTANT ////////////////////////////////
////////////////////////////////////////////////////////////////////////


Route::middleware(['auth','role:consultant'])->group(function () {
    
    Route::get('/home',[MenuController::class,'init'])->name('menu.create');

    Route::get('/Visite/gestion/{id?}',[VisiteController::class,'show'])->where("id","[0-9]+");

    // Entreprise
    Route::get('/Entreprise/gestion/{id?}',[EntrepriseController::class,'show'])->where("id","[0-9]+");

    Route::get('/Entreprise/all',[EntrepriseController::class,'paginate']);
    Route::put('/Entreprise/all',[EntrepriseController::class,'paginate_search']);
    
    // Secteur
    Route::get('/Secteur/all',[SecteurController::class,'paginate']);
    Route::put('/Secteur/all',[SecteurController::class,'paginate_search']);

    // Contact
    Route::get('/Contact/gestion/{id?}',[ContactController::class,'show'])->where("id","[0-9]+");

    Route::get('/Contact/all',[ContactController::class,'paginate']);
    Route::put('/Contact/all',[ContactController::class,'paginate_search']);

    // Secteur
    Route::get('/Secteur/gestion/{id?}',[SecteurController::class,'infos'])->where("id","[0-9]+");

});

require __DIR__.'/auth.php';
