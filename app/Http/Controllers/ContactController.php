<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Entreprise;
use App\Models\Contact;
use App\Models\Visite;

class ContactController extends Controller
{
    public function show($id){
        $contact = Contact::find($id);
        $entreprises = Entreprise::orderBy('EntNom', 'asc')->get();

        $user  = auth()->user(); 
        $droit = DB::select('SELECT role_IdRole FROM role_user WHERE user_id = ?', [$user->id]);
        $droit = count($droit);

        if($droit == 1){
            $isDisabled = "disabled";
        }
        else{
            $isDisabled = "";
        }

        return view('VueContact.GestionContact',['contact'=>$contact,'entreprises'=>$entreprises,'isDisabled'=>$isDisabled,'user'=>$user]);
    }

    public function find($id){
        $contact = Contact::find($id);
        return $contact;
    }

    public function paginate(){
        $contacts = Contact::paginate(11);
        $user  = auth()->user();
        return view('VueContact.Contacts',['contacts'=>$contacts,'user'=>$user]);
    }

    public function paginate_search(){
        $content = Request("search");
        $contacts = Contact::where('ConNom', 'like', '%'.$content.'%')->paginate(11);
        $user  = auth()->user();
        return view('VueContact.Contacts',['contacts'=>$contacts, 'content_search'=>$content,'user'=>$user]);
    }

    public function update($id){
        $contact = ContactController::find($id);
        $contact->ConNom = Request("Nom");
        $contact->ConPrenom = Request("Prenom");
        $contact->ConPoste = Request("Poste");
        $contact->ConTel = Request("Tel");
        $contact->IdEntreprise = Request("Entreprise");
        $contact->save();
        return redirect('Contact/gestion/'.$id.'');
    }

    public function before_create(){
        $entreprises = Entreprise::orderBy('EntNom', 'asc')->get();
        return view('VueContact.NouveauContact',['entreprises'=>$entreprises]);
    }

    public function create(){
        $contact = New Contact;
        $contact->ConNom = Request("Nom");
        $contact->ConPrenom = Request("Prenom");
        $contact->ConPoste = Request("Poste");
        $contact->ConTel = Request("Tel");
        $contact->IdEntreprise = Request("Entreprise");
        $contact->save();
        return redirect('Contact/gestion/'.$contact->IdContact.'');
    }

    public function confirmDelete($id){
        $contact = Contact::find($id);
        return view('VueContact.SuppressionContact',['contact'=>$contact]);
    }

    public function delete($id){
        // Verification contraintes
        $count = Visite::where("IdContact",$id)->get();
        $contact = Contact::find($id);
        if(count($count) > 0){
            return view('VueContact.SuppressionContact',['contact'=>$contact,'this_alert'=> true]);
        }
        else{
            $contact->delete();
            return redirect('Contact/all');
        }
    }
}
