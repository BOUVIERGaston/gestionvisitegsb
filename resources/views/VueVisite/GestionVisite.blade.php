@include('header',['title'=> 'Detail Visite'])
@include('navbar',['title'=> "Modification d'une visite"])
<?php 
use App\Models\User;
$user  = Auth::user(); 
?>

<body>
   
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Aperçu et Modification d'une Visite @if ($user->roles()->where('LibelleRole','commercial')->exists())<a class="trash-right" href="/Visite/Suppression/{{$visite->id}}" {{$isDisabled}}><ion-icon size="large" name="trash-outline"></ion-icon></a>@endif</h5>
               <div class="card-body">
                  <form method='POST' action=''>
                     
                     @method('PUT')
                     @csrf  
                    
                     <label>Entreprise<span class="obligatoire">*</span></label>
                     <div class="input-group">
                        <select class="form-select" name='Entreprise' required {{$isDisabled}}>
                           <option value="{{$visite->IdEntreprise}}" selected>{{$visite->Entreprise->EntNom}}</option>
                        
                           @foreach ($entreprises as $entreprise)
                              @if ($entreprise->IdEntreprise == $visite->IdEntreprise) 
                              @else
                                 <option value="{{$entreprise->IdEntreprise}}">{{$entreprise->EntNom}}</option>
                              @endif
                           @endforeach
                        </select>
                        @if ($user->roles()->where('LibelleRole','commercial')->exists())
                           <button class="btn btn-outline-success" type="button" onclick="window.location.href = '/Entreprise/new'" {{$isDisabled}}>Créer</button>
                        @endif
                     </div>
                     <br>

                     <label>Contact <i>( Format : "Nom entreprise - nom prenom du contact")</i><span class="obligatoire">*</span></label>
                     <div class="input-group">
                        <select class="form-select" name='Contact' required {{$isDisabled}}>
                           <?php
                              use App\Models\Contact;
                              use App\Models\Entreprise;
                              $EntActualContact = Contact::find($visite->IdContact);
                           ?>
                           <option value="{{$visite->IdContact}}" selected>{{$EntActualContact->Entreprise->EntNom." - ".$EntActualContact->ConNom." ".$EntActualContact->ConPrenom}}</option>
                           
                           @foreach ($contacts as $contact)
                           
                              @if ($contact->IdContact == $visite->IdContact)
                                 
                              @else
                                 <?php
                                    $monEntreprise = Entreprise::find($contact->IdEntreprise);
                                 ?>
                                 <option value="{{$contact->IdContact}}">{{$monEntreprise->EntNom." - ".$contact->ConNom." ".$contact->ConPrenom}}</option>
                              @endif
                           @endforeach
                        </select>
                        @if ($user->roles()->where('LibelleRole','commercial')->exists())
                           <button class="btn btn-outline-success" type="button" onclick="window.location.href = '/Contact/new'" {{$isDisabled}}>Créer</button>
                        @endif
                     </div>
                     <br>

                     <label>Date de visite <i>( Format : "AAAA-MM-JJ")</i><span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Date' value="{{ $visite->VisDate }}" required {{$isDisabled}}>
                     <br>
                     
                     <label>Commentaire de la visite</label>
                     <textarea class="form-control" rows="5" name="Comment" {{$isDisabled}}>{{ $visite->VisCommentaire }}</textarea>
                     <br>
                     
                     <div class="text-center">
                        <button type="submit" class="btn btn-primary" {{$isDisabled}}>Valider</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>