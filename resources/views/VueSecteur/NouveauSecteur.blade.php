@include('header',['title'=> 'Nouveau Secteur'])
@include('navbar',['title'=> "Création d'un Secteur"])

<body>

   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Création d'un Secteur</h5>
               <div class="card-body">
                  <form method='POST' action=''>
                     
                     @method('PUT')
                     @csrf  
                     <label>Nom du Secteur</label>
                     <input type="texte" class="form-control" name='Libelle'>
                     <br>
                     <div class="text-center">
                        <button type="submit" class="btn btn-primary">Valider</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>