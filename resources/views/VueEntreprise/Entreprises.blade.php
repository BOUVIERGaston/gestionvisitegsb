@include('header',['title'=> 'Entreprises'])
@include('navbar',['title'=> "Visualisation des Entreprises"])
<?php 
use App\Models\Secteur;
use App\Models\Entreprise;
?>


<body>

   <br>
   <div class="row text-center" id="search-form">
      <form method='POST' action=''>
                     
         @method('PUT')
         @csrf  
         <input type="text" class="form-control" name="search" id="search-input" placeholder="Rechercher par nom" 
         @if(isset($content_search)) value="{{$content_search}}"@endif>
         <button class="btn btn-outline-success" type="submit">Rechercher</button>
         @if ($user->roles()->where('LibelleRole','commercial')->exists())
         <a href="new" class="button_create_table"><button type="button" class="btn btn-secondary">Nouvelle Entreprise</button></a>
         @endif
      </form>
      
   </div>
   <br>
   
   <div class="row justify-content-center">
      <div class="col-1"></div>
      <div class="col-10">
         <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
               <th scope="col">Id Entreprise</th>
               <th scope="col">Nom</th>
               <th scope="col">Ville</th>
               <th scope="col">Adresse</th>
               <th scope="col">Secteur</th>
               <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
               @foreach ($entreprises as $entreprise)
                  <tr>
                     <td scope='row'>{{ $entreprise->IdEntreprise }}</td>
                     <td scope='row'>{{ $entreprise->EntNom }}</td>
                     <td scope='row'>{{ $entreprise->EntVille }}</td>
                     <td scope='row'>{{ $entreprise->EntAdresse }}</td>
                     <?php $secteur = Secteur::find($entreprise->IdSecteur); ?>
                     <td scope='row'><a href="/Secteur/gestion/{{$entreprise->IdSecteur}}">{{ $secteur->SecLibelle }}</a></td>
                     <td scope='row'>
                        <a href="gestion/{{$entreprise->IdEntreprise}}"><button  type="button" class="btn btn-success">Modifier</button></a>
                        @if ($user->roles()->where('LibelleRole','commercial')->exists())
                        <a href="Suppression/{{$entreprise->IdEntreprise}}"><button type="button" class="btn btn-danger">Supprimer</button></a>
                        @endif
                     </td>
                  <tr>
               @endforeach
            </tbody>
         </table>
      </div>
      <div class="col-1"></div>
   </div>
</body>
<footer>
   @if(isset($entreprises))
      <ul class="pagination justify-content-center mb-4">
         {{$entreprises->links("pagination::bootstrap-4")}}
      </ul>
   @endif
</footer>