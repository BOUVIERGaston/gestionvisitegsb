@include('header',['title'=> 'Secteur'])
@include('navbar',['title'=> "Suppression d'un Secteur"])


<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Etes vous sûre de vouloir supprimer ce secteur ?</h5>
               <div class="card-body">
                  Vous êtes sur le point de supprimer le secteur "{{$secteur->SecLibelle}}" (id : {{$secteur->IdSecteur}}).<br>
                  Attention, cette action est irréversible
                  <br><br>
                  <div class="text-center button-suppression">
                     <a href="../gestion/{{$secteur->IdSecteur}}"><button class="btn btn-danger">Retour</button></a>
                     <form method='POST' action=''>
                        @method('PUT')
                        @csrf 
                        <button type="submit" class="btn btn-success">Oui Supprimer</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>

@if (isset($this_alert) == 1)
   
<div class="modal fade show" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterTitle" style="display: block;" aria-modal="true" role="dialog">
   <div class="modal-dialog modal-dialog-centered">
     <div class="modal-content">
       <div class="modal-header">
         <h1 class="modal-title fs-5" id="exampleModalCenterTitle">Ce Secteur ne peut pas être supprimé</h1>
       </div>
       <div class="modal-body">
         <p>
            Ce secteur ne peut pas être supprimé,<br>
            Ce secteur est relié à des entreprises,<br>
            Merci pour votre compréhension
         </p>
       </div>
       <div class="modal-footer justify-content-center">
         <a href="/Secteur/gestion/{{$secteur->IdSecteur}}"><button type="button" class="btn btn-primary">D'accord</button></a>
       </div>
     </div>
   </div>

    <style>
      
      .modal{
         /* opacity:1;   */
         -webkit-backdrop-filter: blur(5px); /* assure la compatibilité avec safari */
         backdrop-filter: blur(5px);  
      }
    </style>
    
@endif