@include('header',['title'=> 'Nouveau contact'])
@include('navbar',['title'=> "Création d'un Contact"])


<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Création d'un contact</h5>
               <div class="card-body">
                  <form method='POST' action=''>
                     
                     @method('PUT')
                     @csrf  
                    
                     <label>Nom<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Nom' required>
                     <br>
                     <label>Prenom<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Prenom' required>
                     <br>
                     <label>Intitulé du poste</label>
                     <input type="texte" class="form-control" name='Poste'>
                     <br>
                     <label>Numéro de téléphone</label>
                     <input type="texte" class="form-control" name='Tel'>
                     
                     <br>
                     <label>Sélection de l'Entreprise<span class="obligatoire">*</span></label>
                     <select class="form-select" aria-label="Default select example" name='Entreprise' required>
                        @foreach ($entreprises as $entreprise)
                              <option value="{{$entreprise->IdEntreprise}}">{{$entreprise->EntNom}}</option>
                        @endforeach
                     </select>
                     <br>
                     <div class="text-center">
                        <button type="submit" class="btn btn-primary">Valider</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>