
<?php 
use App\Models\User;
$user  = Auth::user(); 
?>
<nav class="navbar navbar-light bg-light">
   

   <nav class="navbar navbar-expand bg-body-tertiary" style ="width:100%;">
      <div class="container">
         <h1 class="nav-title">{{$title}}</h1>
         <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
         </button>
      </div>
      <div class="collapse navbar-collapse" id="navbarScroll">
         <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
            
            
            <li class="nav-item dropdown">
               <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Visite
               </a>
               <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="/home">Voir les visites</a></li>
                  @if ($user->roles()->where('LibelleRole','commercial')->exists())
                  <li><a class="dropdown-item" href="/Visite/new">Nouvelle Visite</a></li>
                  @endif
               </ul>
            </li>

            <li class="nav-item dropdown">
               <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Contact
               </a>
               <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="/Contact/all">Voir les Contacts</a></li>
                  @if ($user->roles()->where('LibelleRole','commercial')->exists())
                  <li><a class="dropdown-item" href="/Contact/new">Nouveau Contact</a></li>
                  @endif
               </ul>
            </li>

            <li class="nav-item dropdown">
               <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Entreprise
               </a>
               <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="/Entreprise/all">Voir les Entreprises</a></li>
                  @if ($user->roles()->where('LibelleRole','commercial')->exists())
                  <li><a class="dropdown-item" href="/Entreprise/new">Nouvelle Entreprise</a></li>
                  @endif
               </ul>
            </li>

            
            @if ($user->roles()->where('LibelleRole','admin')->exists())
               <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                     Secteur
                  </a>
                  <ul class="dropdown-menu">
                     <li><a class="dropdown-item" href="/Secteur/all">Voir les Secteurs</a></li>
                     <li><a class="dropdown-item" href="/Secteur/new">Nouveau Secteur</a></li>
                  </ul>
               </li>
            

            <li class="nav-item dropdown">
               <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Administration
               </a>
               <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="/User/all">Voir les Utilisateurs</a></li>
                  <li><a class="dropdown-item" href="/User/register">Créer un nouvel Utilisateur</a></li>
               </ul>
            </li>
            @endif

            <li class="nav-item dropdown">
               <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">{{$user->name}}</a>
               <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="/home">Page d'acceuil</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="/logout">Déconnexion</a></li>
               </ul>
            </li>
            <li></li>
         </ul>
      </div>
    </nav>
</nav>