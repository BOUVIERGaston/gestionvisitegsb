@include('header',['title'=> 'Utilisateur'])
@include('navbar',['title'=> "Suppression d'un Utilisateur"])


<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Etes vous sûre de vouloir supprimer cet Utilisateur ?</h5>
               <div class="card-body">
                  Vous êtes sur le point de supprimer l'utilisateur' "{{$user->name}} - {{$user->email}}".<br>
                  Attention, cette action est irréversible
                  <br><br>
                  <div class="text-center button-suppression">
                     <a href="/User/gestion/{{$user->id}}"><button class="btn btn-danger">Retour</button></a>
                     <form method='POST' action=''>
                        @method('PUT')
                        @csrf 
                        <button type="submit" class="btn btn-success">Oui Supprimer</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>