@include('header',['title'=> 'Nouvelle Entreprise'])
@include('navbar',['title'=> "Création d'une Entreprise"])

<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Création d'une nouvelle entreprise</h5>
               <div class="card-body">
                  <form method='POST' action=''>
                     
                     @method('PUT')
                     @csrf  
                    
                     <label>Nom<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Nom' required>
                     <br>
                     <label>Ville<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Ville' required>
                     <br>
                     <label>Adresse<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Adresse' required>
                     <br>
                     <label>Choisir un secteur<span class="obligatoire">*</span></label>
                     <select class="form-select" aria-label="Default select example" name='Secteur' required>
                        
                        @foreach ($secteurs as $secteur)
                              <option value="{{$secteur->IdSecteur}}">{{$secteur->SecLibelle}}</option>
                        @endforeach
                     </select>
                     <br>
                     <div class="text-center">
                        <button type="submit" class="btn btn-primary">Valider</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>