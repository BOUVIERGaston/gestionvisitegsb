@include('header',['title'=> 'Secteurs'])
@include('navbar',['title'=> "Visualisation des Secteurs"])

<body>

   <br>
   <div class="row text-center" id="search-form">
      <form method='POST' action=''>
                     
         @method('PUT')
         @csrf  
         <input type="text" class="form-control" name="search" id="search-input" placeholder="Rechercher par nom" 
         @if(isset($content_search)) value="{{$content_search}}"@endif>
         <button class="btn btn-outline-success" type="submit">Rechercher</button>
         <a href="new" class="button_create_table"><button type="button" class="btn btn-secondary">Nouveau Secteur</button></a>
      </form>
      
   </div>
   <br>
   <div class="row justify-content-center">
      <div class="col-1"></div>
      <div class="col-10">
         <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
               <th scope="col">Id Secteur</th>
               <th scope="col">Nom du Secteur</th>
               <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
               @foreach ($secteurs as $secteur)
                  <tr>
                     <td scope='row'>{{ $secteur->IdSecteur }}</td>
                     <td scope='row'>{{ $secteur->SecLibelle }}</td>
                     <td scope='row'>
                        <a href="gestion/{{$secteur->IdSecteur}}"><button  type="button" class="btn btn-success">Modifier</button></a>
                        <a href="Suppression/{{$secteur->IdSecteur}}"><button type="button" class="btn btn-danger">Supprimer</button></a>
                     </td>
                  <tr>
               @endforeach
            </tbody>
         </table>
      </div>
      <div class="col-1"></div>
   </div>
</body>
<footer>
   @if(isset($secteurs))
      <ul class="pagination justify-content-center mb-4">
         {{$secteurs->links("pagination::bootstrap-4")}}
      </ul>
   @endif
</footer>