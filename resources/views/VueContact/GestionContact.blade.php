@include('header',['title'=> 'Detail contact'])
@include('navbar',['title'=> "Modification d'un Contact"])


<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Aperçu et Modification du Contact @if ($user->roles()->where('LibelleRole','commercial')->exists())<a class="trash-right" href="/Contact/Supression/{{$contact->IdContact}}"><ion-icon size="large" name="trash-outline"></ion-icon></a>@endif</h5>
               <div class="card-body">
                  <form method='POST' action=''>
                     
                     @method('PUT')
                     @csrf  
                    
                     <label>Id Contact</label>
                     <input type="texte" class="form-control" name='Idcontact' value="{{ $contact->IdContact }}" disabled="disabled">
                     <br>
                     <label>Nom<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Nom' value="{{ $contact->ConNom }}" {{$isDisabled}} required>
                     <br>
                     <label>Prenom<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Prenom' value="{{ $contact->ConPrenom}}" {{$isDisabled}} required>
                     <br>
                     <label>Intitulé du poste<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Poste' value="{{ $contact->ConPoste }}" {{$isDisabled}} required>
                     <br>
                     <label>Numéro de téléphone<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Tel' value="{{ $contact->ConTel }}" {{$isDisabled}} required>
                     
                     <br>
                     <label>Entreprise<span class="obligatoire">*</span></label>
                     <select class="form-select" aria-label="Default select example" name='Entreprise' {{$isDisabled}} required>
                        <option value="{{$contact->IdEntreprise}}" selected>{{$contact->Entreprise->EntNom}}</option>
                        
                        @foreach ($entreprises as $entreprise)
                        
                           @if ($entreprise->IdEntreprise == $contact->IdEntreprise)
                              
                           @else
                              <option value="{{$entreprise->IdEntreprise}}">{{$entreprise->EntNom}}</option>
                           @endif
                        @endforeach
                     </select>
                     <br>
                     <div class="text-center">
                        <button type="submit" class="btn btn-primary" {{$isDisabled}}>Valider</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>