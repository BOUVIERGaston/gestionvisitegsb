@include('header',['title'=> 'Utilisateur'])
@include('navbar',['title'=> "Modification d'un Utilisateur"])


<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Aperçu et Modification d'un Utilisateur<a class="trash-right" href="/User/Suppression/{{$user->id}}"><ion-icon size="large" name="trash-outline"></ion-icon></a></h5>
               <div class="card-body">
                  <form method='POST' action=''>
                     
                     @method('PUT')
                     @csrf  
                    
                     <label>Nom</label>
                     <input type="texte" class="form-control" name='name' value="{{$user->name}}" required>
                     <br>
                     <label>Email</label>
                     <input type="texte" class="form-control" name='email' value="{{ $user->email}}" required>
                     <br>
                     <label>Mot de passe <i></i></label>
                     <input type="password" class="form-control" name='password'>
                     <br>
                     <label>Confirmation du mot de passe</label>
                     <input type="password" class="form-control" name='confirm-password'">
                     <br>
                     <label>Role</label>
                     <select class="form-select" name='droit'>
                        @if($droit == 1)
                           <option value="1" selected>Consultant</option>
                           <option value="2">Commercial</option>
                           <option value="3">Administrateur</option>
                        @elseif ($droit == 2)
                           <option value="1">Consultant</option>
                           <option value="2" selected>Commercial</option>
                           <option value="3">Administrateur</option>
                        @else
                           <option value="1">Consultant</option>
                           <option value="2">Commercial</option>
                           <option value="3" selected>Administrateur</option>
                        @endif
                        
                     </select>
                     <br>
                     
                     <div class="text-center">
                        <button type="submit" class="btn btn-primary">Valider</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>