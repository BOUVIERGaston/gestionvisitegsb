@include('header',['title'=> 'Secteur'])
@include('navbar',['title'=> "Modification d'un Secteur"])


<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Aperçu et Modification du Secteur @if ($user->roles()->where('LibelleRole','admin')->exists())<a class="trash-right" href="/Secteur/Suppression/{{$secteur->IdSecteur}}"><ion-icon size="large" name="trash-outline"></ion-icon></a>@endif</h5>
               <div class="card-body">
                  <form method='POST' action=''>
                     
                     @method('PUT')
                     @csrf  
                     <label>Id Secteur</label>    
                     <input type="texte" class="form-control" name='IdSecteur' value="{{ $secteur->IdSecteur }}" disabled="disabled">
            
                     <label>Nom du Secteur<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Libelle' value="{{ $secteur->SecLibelle }}" {{$isDisabled}} required>
                     <br>
                     <div class="text-center">
                        <button type="submit" class="btn btn-primary" {{$isDisabled}}>Valider</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>