
@include('header',['title'=> 'Menu'])
@include('navbar',['title'=> 'Visualisation des visites'])

<?php
   use App\Models\Contact;
   use App\Models\Entreprise;
   use App\Models\User;
   $user  = Auth::user(); 
?>
<body>
   <div class="container-fluid">   
      <div class="col-12">
         <table class="table table-striped" id="table_espace_utilisateur">
            <thead>
               <th scope="col">Entreprise</th>
               <th scope="col">Date</th>
               <th scope="col">Contact</th>
               <th scope="col">Commentaire</th>
               <th scope="col">
                  Actions 
                  @if ($user->roles()->where('LibelleRole','commercial')->exists())
                     <button type="button" class="btn btn-success" href="">+</button>
                  @endif
               </th>
            </thead>
            @foreach ($visites as $visite)
               <tr>
                  <td ><a href='Entreprise/gestion/{{$visite->IdEntreprise}}'>{{ $visite->Entreprise->EntNom}}</a></td>
                  <td >{{ $visite->VisDate}}</td>
                  <td >
                     <?php $contact = Contact::find($visite->IdContact) ?>
                     <a href='Contact/gestion/{{$contact->IdContact}}'>{{$contact->ConNom}} - {{$contact->ConPrenom}}</a>
                  </td>
                  @if (strlen($visite->VisCommentaire)>50)
                     <td >{{substr($visite->VisCommentaire, 0, 49)."..."}}</td>
                  @else
                     <td >{{ $visite->VisCommentaire}}</td>
                  @endif

                  <td><a href="/Visite/gestion/{{$visite->id}}"><button type="button" class="btn btn-primary">Voir plus</button></a></td>
               <tr>
            @endforeach
         </table>
      </div>
      <div class="col-12">
         <ul class="pagination justify-content-center mb-4">
            {{$visites->links("pagination::bootstrap-4")}}
         </ul>
      </div>
   </div>
</body>