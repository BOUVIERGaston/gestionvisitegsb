-- MariaDB dump 10.19  Distrib 10.5.16-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: gestionvisite_db
-- ------------------------------------------------------
-- Server version	10.5.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CONTACT`
--

DROP TABLE IF EXISTS `CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTACT` (
  `IdContact` int(11) NOT NULL AUTO_INCREMENT,
  `ConNom` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `ConPrenom` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `ConPoste` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `ConTel` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `IdEntreprise` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdContact`),
  KEY `IdEntreprise` (`IdEntreprise`),
  CONSTRAINT `CONTACT_ibfk_1` FOREIGN KEY (`IdEntreprise`) REFERENCES `ENTREPRISE` (`IdEntreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CONTACT`
--

LOCK TABLES `CONTACT` WRITE;
/*!40000 ALTER TABLE `CONTACT` DISABLE KEYS */;
INSERT INTO `CONTACT` VALUES (1,'Bronson','Mike','DRH','0680311125',2),(2,'Foker','Rice',NULL,'6075705415',25),(3,'Couves','Nilson','Responsable Fournitures','5314240101',40),(4,'Challicum','Logan','Responsable Fournitures','7231152011',8),(5,'MacCoveney','Ringo','PDG','5338648291',43),(6,'Duchant','Kain','Responsable Fournitures','4158946136',3),(7,'Fitzjohn','Claudian','Responsable Fournitures','4878625021',2),(8,'Emson','James','Responsable Fournitures','1175689313',47),(9,'Hegdonne','Tanner','PDG','4929500778',39),(10,'Willets','Kippar',NULL,'7199866196',9),(11,'Mattiuzzi','Clarke',NULL,'5009210511',4),(12,'Siddeley','Lanie',NULL,'1428667041',49),(13,'Loakes','Hadlee',NULL,'6211617934',25),(14,'Rippin','Hamid','PDG','3218017452',17),(15,'Phair','Hasheem',NULL,'6735792315',7),(16,'Boulter','Saxon',NULL,'3365188173',29),(17,'Mellor','Ezechiel',NULL,'6781100896',18),(18,'Bushby','Sutherland','PDG','8183682263',45),(19,'Galbreath','Waverley',NULL,'9124165050',6),(20,'MacArthur','Danny','PDG','7431229543',33),(21,'Willimont','Giusto',NULL,'1907064948',1),(22,'Delmonti','Connor','PDG','7404589179',46),(23,'Di Ruggero','Ansel',NULL,'7873628742',25),(24,'Kettlesing','Augustin',NULL,'4899796572',46),(25,'Edwicke','Ethelred','PDG','9789153211',45),(26,'Klimmek','Vernen',NULL,'7358212246',34),(27,'Pesselt','Vittorio',NULL,'6053063598',19),(28,'Danilchik','Somerset','PDG','6907190212',26),(29,'Swansbury','Torr','PDG','4142991783',26),(30,'Danielkiewicz','Frasier',NULL,'5878252143',30),(31,'Corley','Ivan','PDG','1993219065',49),(32,'Melan','Alexandre','PDG','6028716706',44),(33,'Dufour','Antonin','PDG','6156710752',32),(34,'Fenning','Filberto',NULL,'1081466865',39),(35,'Corkan','Wylie',NULL,'4433552187',47),(36,'Riatt','Levin',NULL,'3057820124',46),(37,'Heaney','Liam',NULL,'2254964482',38),(38,'Thaxter','Gibbie',NULL,'3863740538',28),(39,'Hudleston','Alden',NULL,'6909584325',33),(40,'Porter','Emlyn',NULL,'2451961306',18),(41,'Trathan','Jabez',NULL,'3151464863',39),(42,'Stolberg','Phillip',NULL,'6068611783',16),(43,'Paterson','Blayne',NULL,'1644203868',30),(44,'Petr','Lennie',NULL,'2192297419',11),(45,'Amiable','Otto',NULL,'9992946380',29),(46,'Rodders','Guillermo',NULL,'8184677500',36),(47,'McTerlagh','Nathanil',NULL,'8344040277',38),(48,'Millington','Gun',NULL,'4957395038',5),(49,'Messenger','Gaultiero',NULL,'3209209556',30),(50,'Garatty','Dukey',NULL,'1267044216',35),(51,'Scouse','Cletus',NULL,'1402457516',25),(52,'Deek','Chip',NULL,'1558834954',25),(53,'Joesbury','Lutero',NULL,'3072508917',16),(54,'McBride','Sylvan',NULL,'7113230654',32),(55,'Duffer','Giuseppe',NULL,'2594444125',34),(56,'Abrahamowitcz','Thorsten',NULL,'2281148690',37),(57,'Stodhart','Symon',NULL,'6945306251',16),(58,'Titheridge','Zackariah',NULL,'2664738926',48),(59,'Whylie','Penrod',NULL,'5557182809',6),(60,'Rottenbury','Si',NULL,'8731940727',11),(61,'Aylesbury','Tadeas',NULL,'5379038504',36),(62,'Ludvigsen','Ely',NULL,'5022942123',40),(63,'Huddles','Antoine',NULL,'4495299499',46),(64,'Culligan','Amery',NULL,'4906178574',47),(65,'Pelman','Nate',NULL,'3407707744',32),(66,'Rey','Yehudit',NULL,'1905784415',40),(67,'Trippack','Emmett',NULL,'6885610129',23),(68,'Collingdon','Samson',NULL,'6439098960',50),(69,'Giacomasso','Drew',NULL,'7925996668',35),(70,'Leabeater','Tobe',NULL,'4957878829',42),(71,'Dunrige','Hasty',NULL,'3596373632',36),(72,'Bryde','Demetris',NULL,'1334074174',35),(73,'O\'Heffernan','Tom',NULL,'9196861921',47),(74,'Dripp','Cart',NULL,'8106098274',39),(75,'Leatham','Gregorius',NULL,'5175640189',22),(76,'Leadbitter','Merrel',NULL,'1902846737',35),(77,'Abrahamson','Duffie',NULL,'8527747873',8),(78,'Plitz','Etienne',NULL,'4664208197',26),(79,'Urch','Odey',NULL,'1443020399',29),(80,'Mussalli','Doyle',NULL,'5507264113',36),(81,'Lartice','Timotheus',NULL,'9782078603',27),(82,'De La Haye','Neddie',NULL,'5161771280',31),(83,'Barnby','Sterling',NULL,'1616221352',42),(84,'Fardell','Bill',NULL,'4149620388',27),(85,'Lantiffe','Paolo',NULL,'1702879190',31),(86,'Entwistle','Hillier',NULL,'2401136072',41),(87,'Martinyuk','Conrade',NULL,'1702951414',11),(88,'Raye','Ber',NULL,'2494166327',34),(89,'Coffee','Clarance',NULL,'2479791204',9),(90,'Carman','Renato',NULL,'7565381327',11),(91,'Hainning','Ignace',NULL,'1557452622',38),(92,'Wenderoth','Brocky',NULL,'1563652387',33),(93,'Dmtrovic','Perry',NULL,'7062124587',15),(94,'Dendon','Skelly',NULL,'7821636025',5),(95,'Whylie','Rad',NULL,'1833729947',31),(96,'Gerding','Kennie',NULL,'7231381565',42),(97,'Caillou','Matthieu',NULL,'8114889794',44),(98,'Lamcken','Sergio',NULL,'9129823984',41),(99,'Prangley','Tyrone',NULL,'6335747141',19),(100,'Betchley','Skyler',NULL,'6881071909',6),(103,'nouveau','nouveau',NULL,NULL,33);
/*!40000 ALTER TABLE `CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ENTREPRISE`
--

DROP TABLE IF EXISTS `ENTREPRISE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ENTREPRISE` (
  `IdEntreprise` int(11) NOT NULL AUTO_INCREMENT,
  `EntNom` varchar(300) COLLATE latin1_general_ci DEFAULT NULL,
  `EntVille` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `EntAdresse` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdSecteur` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdEntreprise`),
  KEY `IdSecteur` (`IdSecteur`),
  CONSTRAINT `ENTREPRISE_ibfk_1` FOREIGN KEY (`IdSecteur`) REFERENCES `SECTEUR` (`IdSecteur`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ENTREPRISE`
--

LOCK TABLES `ENTREPRISE` WRITE;
/*!40000 ALTER TABLE `ENTREPRISE` DISABLE KEYS */;
INSERT INTO `ENTREPRISE` VALUES (1,'RajareCorp','Lyon','28 rue Pierre Dupont',9),(2,'Spencer, Greenholt and Collins','Sangmélima','5 Corben Crossing',9),(3,'Howell-Lang','Seidu','99643 Cardinal Crossing',2),(4,'Schowalter Group','Lyon','25 Cordelia Street',1),(5,'Kessler-Schroeder','Allen','65219 Kim Drive',8),(6,'Kilback, Quitzon and Rosenbaum','Antas','58886 Summer Ridge Court',7),(7,'Pacocha-O\'Conner','Pamotan','45484 7th Plaza',7),(8,'Klein-Kerluke','Kasulu','073 Warbler Way',2),(9,'Gibson and Sons','Jaguariúna','3 Evergreen Court',1),(10,'Pfeffer-Powlowski','Fort-de-France','47437 Kings Street',5),(11,'Schulist Inc','Mardakyany','95372 School Plaza',9),(12,'Walter-Simonis','Diaopu','6999 Iowa Crossing',4),(13,'Heaney-Hagenes','Itororó','6118 Vernon Drive',6),(14,'Bauch, Williamson and Wyman','‘Ayn an Nasr','749 Charing Cross Drive',4),(15,'Hane Inc','Villeurbanne','808 Maple Wood Park',5),(16,'Homenick-Tremblay','Wanxian','56 Debra Lane',1),(17,'Hahn LLC','Atlanta','0310 Del Mar Point',5),(18,'Tillman LLC','Humaitá','8 Esker Alley',2),(19,'Veum LLC','Reims','4414 Steensland Trail',8),(20,'Kertzmann-Wisoky','Sarykemer','93938 Manley Point',9),(21,'McGlynn, Stokes and Langosh','Znamenskoye','7086 Morningstar Way',2),(22,'Simonis, Crona and Aufderhar','San Carlos','3052 Helena Trail',2),(23,'Gislason-Dietrich','Yahil’nytsya','4012 Hayes Way',7),(24,'Abernathy and Sons','Lalmanirhat','777 Debs Parkway',4),(25,'Powlowski, Nitzsche and Johnston','Petauke','0 8th Place',7),(26,'Thiel and Sons','San Francisco','71934 Spaight Terrace',7),(27,'Lemke-Schmidt','Zala','015 Bartillon Parkway',6),(28,'Murazik-Waelchi','Bron','2294 Barby Alley',5),(29,'Brown Group','Cisompet','5780 Vera Terrace',4),(30,'Crona Inc','Dijon','360 Bluejay Drive',8),(31,'Williamson-Volkman','Paris 12','21771 Grim Hill',3),(32,'Mosciski, Trantow and Hegmann','Cheb','20 Nobel Way',9),(33,'Wintheiser, Schmitt and Donnelly','Vynohradivka','36 Butterfield Junction',2),(34,'Reynolds, Nikolaus and Dickens','Wasagu','623 3rd Place',7),(35,'Hettinger-Towne','Castries','5603 Lindbergh Alley',8),(36,'Christiansen-Nitzsche','Arklow','529 Red Cloud Lane',3),(37,'Vandervort-Schroeder','Jiwei','0432 Sullivan Pass',3),(38,'Willms-Jenkins','Trollhättan','42941 Sugar Point',3),(39,'Stanton and Sons','Leigongjian','114 Killdeer Plaza',4),(40,'Beer-Jones','Karatsu','6492 Sage Way',4),(41,'Stoltenberg and Sons','Wuhu','588 Springs Road',8),(42,'Dicki Inc','Lamadelaine','19 Homewood Road',2),(43,'Gleichner-Botsford','São Mateus','193 Morrow Court',9),(44,'Luettgen-Swift','Al Muharraq','71 Columbus Terrace',8),(45,'Cartwright, Legros and Homenick','Castanheiro do Sul','36 Thierer Place',5),(46,'Schmitt, Smith and Schaden','Marseille','2 Melby Center',7),(47,'Pagac Inc','Johor Bahru','1953 Autumn Leaf Terrace',9),(48,'Bahringer-Hettinger','Krzczonów','534 Susan Junction',2),(49,'McGlynn, Hessel and Huels','Pangtangis Dajah','01214 Kim Center',9),(50,'Lang Inc','Swindon','1200 Longview Pass',8),(53,'nouvelle','Lyon','adresse',1),(54,'nouvelle','Lyon','adresse',1);
/*!40000 ALTER TABLE `ENTREPRISE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `IdRole` int(11) NOT NULL AUTO_INCREMENT,
  `LibelleRole` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`IdRole`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Role`
--

LOCK TABLES `Role` WRITE;
/*!40000 ALTER TABLE `Role` DISABLE KEYS */;
INSERT INTO `Role` VALUES (1,'consultant'),(2,'commercial'),(3,'admin');
/*!40000 ALTER TABLE `Role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SECTEUR`
--

DROP TABLE IF EXISTS `SECTEUR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SECTEUR` (
  `IdSecteur` int(11) NOT NULL AUTO_INCREMENT,
  `SecLibelle` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdSecteur`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SECTEUR`
--

LOCK TABLES `SECTEUR` WRITE;
/*!40000 ALTER TABLE `SECTEUR` DISABLE KEYS */;
INSERT INTO `SECTEUR` VALUES (1,'Bretagne'),(2,'Bourgogne'),(3,'Var'),(4,'Aquitaine'),(5,'Loire'),(6,'Hérault'),(7,'Savoie'),(8,'Auvergne'),(9,'Rhône'),(15,'Hérault'),(16,'Landes'),(17,'Gironde');
/*!40000 ALTER TABLE `SECTEUR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER`
--

DROP TABLE IF EXISTS `USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USER` (
  `IdUser` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `UMail` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `UPassword` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `UNom` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `UPrenom` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `URights` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`IdUser`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER`
--

LOCK TABLES `USER` WRITE;
/*!40000 ALTER TABLE `USER` DISABLE KEYS */;
INSERT INTO `USER` VALUES (1,'g.bouvier@eleve.leschartreux.net','123+Aze','Bouvier','Gaston','0');
/*!40000 ALTER TABLE `USER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VISITE`
--

DROP TABLE IF EXISTS `VISITE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VISITE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdEntreprise` int(11) NOT NULL,
  `VisDate` date NOT NULL,
  `IdContact` int(11) NOT NULL,
  `VisCommentaire` varchar(450) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `VISITE_FK` (`IdContact`),
  KEY `VISITE_FK_1` (`IdEntreprise`),
  CONSTRAINT `VISITE_FK` FOREIGN KEY (`IdContact`) REFERENCES `CONTACT` (`IdContact`),
  CONSTRAINT `VISITE_FK_1` FOREIGN KEY (`IdEntreprise`) REFERENCES `ENTREPRISE` (`IdEntreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VISITE`
--

LOCK TABLES `VISITE` WRITE;
/*!40000 ALTER TABLE `VISITE` DISABLE KEYS */;
INSERT INTO `VISITE` VALUES (1,45,'2022-10-07',18,'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\r\n\r\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.'),(2,40,'2023-06-01',3,'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\r\nTexte généré aléatoirement'),(3,23,'2022-06-18',5,'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\n\nFusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.'),(4,26,'2022-06-12',27,'In congue. Etiam justo. Etiam pretium iaculis justo.'),(5,14,'2022-12-23',31,'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.'),(6,23,'2022-09-26',94,'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.'),(7,24,'2022-07-01',2,'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\n\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.'),(9,6,'2022-06-26',15,'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'),(10,38,'2023-02-04',25,'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.'),(11,24,'2022-07-28',48,'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.'),(12,47,'2023-01-07',62,'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(13,17,'2022-09-13',43,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\n\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.\n\nDuis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.'),(14,34,'2022-08-24',77,'Phasellus in felis. Donec semper sapien a libero. Nam dui.\n\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.'),(15,24,'2022-10-22',13,'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.'),(16,26,'2022-10-26',63,'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.'),(17,16,'2022-11-05',45,'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.'),(18,36,'2022-09-28',47,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.'),(19,41,'2022-08-09',29,'In congue. Etiam justo. Etiam pretium iaculis justo.\n\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\n\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.'),(20,11,'2023-01-07',33,'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.'),(21,35,'2023-01-22',18,'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.'),(22,2,'2022-07-17',92,'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.'),(23,42,'2022-09-21',16,'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\n\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.'),(24,15,'2022-07-19',38,'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.'),(25,8,'2022-11-18',90,'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.'),(26,39,'2022-12-22',61,'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\n\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.'),(28,7,'2022-12-26',81,'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'),(29,22,'2022-07-24',76,'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.'),(30,49,'2022-09-12',53,'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.'),(31,22,'2023-02-12',54,'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.'),(32,38,'2022-11-11',93,'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.'),(33,45,'2023-02-21',26,'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.'),(34,17,'2023-01-20',24,'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.'),(35,19,'2022-11-12',30,'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.'),(36,19,'2023-02-11',84,'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.\n\nIn quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.'),(37,2,'2023-02-04',37,'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.'),(38,18,'2022-12-05',39,'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.'),(39,19,'2022-09-08',55,'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.'),(40,14,'2023-01-27',64,'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\n\nFusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.'),(41,47,'2022-07-10',94,'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'),(42,8,'2022-08-23',22,'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.'),(43,21,'2022-07-14',3,'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.'),(44,20,'2022-12-04',4,'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.'),(45,17,'2022-11-17',25,'Fusce consequat. Nulla nisl. Nunc nisl.'),(46,31,'2022-12-18',33,'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.'),(47,21,'2023-01-31',88,'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\n\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.'),(48,1,'2022-06-17',33,'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'),(49,24,'2022-11-27',20,'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.'),(50,37,'2022-08-08',41,'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(51,23,'2023-02-19',80,'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.'),(52,24,'2023-01-31',24,'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\r\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\r\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.'),(53,35,'2022-11-10',54,'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.'),(54,3,'2023-01-13',55,'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.'),(55,3,'2022-12-28',45,'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\n\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.'),(56,47,'2022-09-15',43,'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.'),(57,49,'2022-06-19',35,'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.'),(58,1,'2022-06-26',92,'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\n\nMorbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\n\nFusce consequat. Nulla nisl. Nunc nisl.'),(59,47,'2023-02-12',92,'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.'),(60,47,'2022-06-09',30,'Sed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.'),(61,23,'2022-10-03',43,'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.\n\nIn quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.'),(62,31,'2022-10-17',71,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\n\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.'),(63,42,'2022-07-21',80,'Fusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.'),(64,42,'2022-11-03',87,'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.'),(65,44,'2022-06-26',46,'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'),(66,22,'2022-11-06',41,'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.'),(67,34,'2022-06-27',78,'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(68,34,'2022-06-05',36,'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.'),(69,44,'2022-11-12',59,'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\n\nPhasellus in felis. Donec semper sapien a libero. Nam dui.'),(70,12,'2022-06-07',48,'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.'),(71,27,'2022-08-04',54,'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(72,27,'2022-10-14',5,'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.'),(73,34,'2023-02-28',48,'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.'),(74,15,'2023-02-08',1,'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.'),(75,27,'2022-10-05',14,'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'),(76,1,'2022-10-17',71,'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.'),(77,42,'2022-11-16',8,'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.'),(78,28,'2022-10-06',16,'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\n\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.'),(79,1,'2022-11-21',89,'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(80,2,'2023-02-11',35,'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.'),(81,11,'2022-11-26',84,'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.'),(84,43,'2022-10-24',10,'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.\n\nIn quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.'),(85,41,'2022-11-07',50,'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.'),(86,29,'2022-08-04',60,'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.'),(87,21,'2022-12-19',90,'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.'),(88,25,'2023-01-16',74,'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.'),(89,17,'2022-10-03',15,'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.'),(90,14,'2023-01-10',8,'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\n\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.'),(91,36,'2022-06-21',57,'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.'),(92,43,'2022-09-14',23,'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.'),(93,47,'2023-01-30',23,'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\n\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.'),(94,5,'2022-06-23',95,'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.'),(95,37,'2022-10-20',35,'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.'),(96,20,'2022-10-04',56,'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\n\nPhasellus in felis. Donec semper sapien a libero. Nam dui.'),(97,1,'2022-10-18',23,'In congue. Etiam justo. Etiam pretium iaculis justo.'),(98,19,'2022-08-01',3,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.'),(99,49,'2022-10-10',57,'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\n\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.'),(100,1,'2022-08-11',42,'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'),(101,48,'2023-06-01',58,'aucun');
/*!40000 ALTER TABLE `VISITE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VISITE_OLD`
--

DROP TABLE IF EXISTS `VISITE_OLD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VISITE_OLD` (
  `id` int(11) DEFAULT NULL,
  `IdEntreprise` int(11) NOT NULL,
  `VisDate` date NOT NULL,
  `IdContact` int(11) NOT NULL,
  `IdUser` bigint(20) unsigned DEFAULT NULL,
  `VisCommentaire` varchar(450) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdEntreprise`,`VisDate`,`IdContact`),
  KEY `IdUser` (`IdUser`),
  KEY `IdContact` (`IdContact`),
  CONSTRAINT `VISITE_OLD_ibfk_2` FOREIGN KEY (`IdEntreprise`) REFERENCES `ENTREPRISE` (`IdEntreprise`),
  CONSTRAINT `VISITE_OLD_ibfk_3` FOREIGN KEY (`IdContact`) REFERENCES `CONTACT` (`IdContact`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VISITE_OLD`
--

LOCK TABLES `VISITE_OLD` WRITE;
/*!40000 ALTER TABLE `VISITE_OLD` DISABLE KEYS */;
INSERT INTO `VISITE_OLD` VALUES (NULL,1,'2021-06-17',18,1,'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.'),(NULL,1,'2022-07-12',2,1,'aucun'),(NULL,2,'2022-06-12',2,1,'aucun'),(NULL,3,'2020-03-03',74,9,'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.'),(NULL,3,'2020-11-16',10,4,'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.'),(NULL,3,'2022-01-08',94,7,'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.'),(NULL,3,'2022-09-15',57,5,'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.'),(NULL,4,'2021-05-22',49,3,'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.'),(NULL,4,'2022-08-12',2,1,'aucun'),(NULL,5,'2021-04-03',22,5,'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.'),(NULL,5,'2021-09-17',79,6,'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.'),(NULL,5,'2022-12-14',2,1,'aucun'),(NULL,6,'2022-09-24',16,2,'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.'),(NULL,7,'2020-05-28',37,5,'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.'),(NULL,7,'2020-12-28',94,2,'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.'),(NULL,7,'2021-01-06',97,5,'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'),(NULL,7,'2022-07-10',82,3,'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),(NULL,7,'2022-10-24',91,8,'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.'),(NULL,8,'2022-07-16',46,3,'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.'),(NULL,8,'2022-08-04',35,2,'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.'),(NULL,10,'2020-06-01',14,6,'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.'),(NULL,10,'2022-11-14',99,6,'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(NULL,12,'2020-06-16',13,6,'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.'),(NULL,13,'2022-07-01',77,5,'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.'),(NULL,14,'2022-10-13',88,1,'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(NULL,15,'2020-03-15',69,4,'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.'),(NULL,15,'2022-03-14',7,8,'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.'),(NULL,16,'2021-03-14',73,6,'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.'),(NULL,16,'2022-05-31',65,1,'Fusce consequat. Nulla nisl. Nunc nisl.'),(NULL,17,'2020-04-02',90,9,'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'),(NULL,17,'2020-05-11',71,5,'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.'),(NULL,18,'2020-05-18',81,1,'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.'),(NULL,18,'2020-12-03',40,9,'Sed ante. Vivamus tortor. Duis mattis egestas metus.'),(NULL,18,'2021-09-03',51,1,'In congue. Etiam justo. Etiam pretium iaculis justo.'),(NULL,18,'2021-12-16',98,5,'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.'),(NULL,18,'2022-07-11',13,5,'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.'),(NULL,19,'2022-07-02',61,6,'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.'),(NULL,19,'2022-09-14',62,6,'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.'),(NULL,19,'2022-12-24',93,2,'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(NULL,20,'2022-04-12',39,5,'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.'),(NULL,20,'2022-07-18',79,4,'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.'),(NULL,21,'2020-12-09',56,9,'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.'),(NULL,21,'2022-02-11',65,9,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.'),(NULL,22,'2022-04-03',10,7,'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'),(NULL,23,'2021-02-14',12,4,'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.'),(NULL,23,'2021-12-01',68,6,'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.'),(NULL,23,'2022-02-12',30,5,'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.'),(NULL,23,'2022-03-15',51,1,'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.'),(NULL,23,'2022-11-27',69,7,'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.'),(NULL,24,'2022-05-07',62,5,'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.'),(NULL,24,'2022-09-21',40,2,'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.'),(NULL,25,'2020-08-20',13,6,'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.'),(NULL,26,'2022-01-21',80,4,'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'),(NULL,27,'2021-08-12',29,5,'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.'),(NULL,27,'2021-11-12',60,8,'Fusce consequat. Nulla nisl. Nunc nisl.'),(NULL,29,'2020-08-23',24,5,'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.'),(NULL,29,'2022-07-05',18,1,'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.'),(NULL,30,'2022-05-21',61,4,'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.'),(NULL,30,'2022-09-05',25,6,'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.'),(NULL,30,'2022-11-18',99,9,'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.'),(NULL,32,'2020-07-08',31,1,'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.'),(NULL,32,'2021-05-07',30,9,'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.'),(NULL,32,'2022-08-10',54,6,'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.'),(NULL,32,'2022-11-26',77,1,'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.'),(NULL,33,'2021-08-22',24,4,'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.'),(NULL,33,'2022-11-15',73,7,'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.'),(NULL,33,'2022-11-23',50,4,'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.'),(NULL,34,'2020-06-14',65,5,'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.'),(NULL,34,'2021-09-05',30,1,'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.'),(NULL,34,'2021-12-24',66,5,'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.'),(NULL,35,'2022-11-05',15,2,'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.'),(NULL,36,'2021-06-18',39,2,'Fusce consequat. Nulla nisl. Nunc nisl.'),(NULL,36,'2021-12-06',11,5,'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.'),(NULL,37,'2020-05-16',70,2,'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.'),(NULL,37,'2022-12-02',15,4,'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.'),(NULL,38,'2021-07-05',64,1,'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.'),(NULL,39,'2021-01-03',46,5,'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(NULL,40,'2020-04-28',96,1,'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),(NULL,40,'2020-09-03',10,9,'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.'),(NULL,40,'2022-02-11',90,5,'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.'),(NULL,42,'2020-04-25',28,9,'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.'),(NULL,42,'2020-12-04',49,1,'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.'),(NULL,42,'2021-03-22',13,6,'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.'),(NULL,42,'2022-12-20',62,1,'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.'),(NULL,43,'2020-04-18',77,5,'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.'),(NULL,43,'2021-10-28',72,8,'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.'),(NULL,43,'2021-12-01',11,9,'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.'),(NULL,43,'2022-03-04',43,2,'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.'),(NULL,43,'2022-05-28',12,7,'Phasellus in felis. Donec semper sapien a libero. Nam dui.'),(NULL,44,'2022-03-28',70,5,'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.'),(NULL,44,'2022-06-04',51,7,'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.'),(NULL,45,'2021-09-13',69,3,'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.'),(NULL,45,'2022-06-09',35,6,'Fusce consequat. Nulla nisl. Nunc nisl.'),(NULL,45,'2022-11-20',43,9,'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.'),(NULL,46,'2020-10-11',19,7,'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.'),(NULL,46,'2022-01-28',55,8,'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.'),(NULL,46,'2022-03-10',14,2,'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.'),(NULL,47,'2020-12-17',71,3,'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.'),(NULL,47,'2022-09-11',57,6,'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.'),(NULL,47,'2022-11-24',97,4,'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'),(NULL,48,'2021-02-20',39,3,'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.'),(NULL,48,'2022-01-25',21,8,'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.'),(NULL,49,'2021-01-15',22,3,'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.'),(NULL,49,'2022-02-10',61,6,'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.');
/*!40000 ALTER TABLE `VISITE_OLD` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `user_id` int(11) NOT NULL,
  `role_IdRole` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_IdRole`),
  KEY `Role_User_IdUser_IDX` (`role_IdRole`,`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1),(1,2),(1,3),(2,1),(9,1),(10,1),(10,2),(11,1),(11,2),(13,1);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Bouvier','admin@admin.com',NULL,'$2y$10$XV50IxN9uAm/6nyRybufXOxroDhMDlYgM3diJfBzsLdA37tmspyQ2',NULL,'2023-01-05 14:29:47','2023-01-05 14:29:47'),(2,'user','user@user.com',NULL,'$2y$10$ZebRcmXQye0LOmapvlNskuxKNFkkOILlW/ZUfMIsxcU3NYFqNwcHi',NULL,'2023-04-14 13:28:10','2023-04-14 13:28:10'),(9,'user_test','test_user@gmail.com',NULL,'$2y$10$npbI6yzTSPTJ0EXFt5OH1OZj4pSWIpX91jBLkMSTwYASPaRW/Skwa',NULL,'2023-06-02 11:05:58','2023-06-02 11:05:58'),(11,'essaidroits','essaidroits@gmail.com',NULL,'$2y$10$e6rAHnjEPuSJp7g2mmrfT.WWIiDfj.VQUJrlhhnH4oZTO3Snwo/wa',NULL,'2023-06-02 11:09:35','2023-06-02 11:09:35');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'gestionvisite_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-06 16:16:17
