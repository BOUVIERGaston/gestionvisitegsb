<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth;

class LoginController extends Controller
{
    function __construct() {
    
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');
    
        if (auth()->attempt($credentials)) {
            return redirect()->intended('dashboard');
        }
        return redirect()->back()->withInput($request->only('email'));
    }

    
    // public function logout(){
    //     auth()->logout();
    //     session()->flush();
    //     session()->regenerate();
    
    //     return redirect('/home');
    // }
        
}
