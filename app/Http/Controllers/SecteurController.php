<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Secteur;
use App\Models\Entreprise;


class SecteurController extends Controller
{
    public static function findByName(string $name){
        $secteur = Secteur::where('SecLibelle', $name)->first(); //->get()
        return $secteur->IdSecteur;
    }

    public function find($id){
        $secteur = Secteur::find($id);
        return $secteur;
    }

    public function paginate(){
        $secteurs = Secteur::paginate(11);
        return view('VueSecteur.Secteurs',['secteurs'=>$secteurs]);
    }

    public function paginate_search(){
        $content = Request("search");
        $secteurs = Secteur::where('SecLibelle', 'like', '%'.$content.'%')->paginate(11);
        return view('VueSecteur.Secteurs',['secteurs'=>$secteurs, 'content_search'=>$content]);
    }

    public function infos($id){
        $secteur = Secteur::find($id);

        $user  = auth()->user(); 
        $droit = DB::select('SELECT role_IdRole FROM role_user WHERE user_id = ?', [$user->id]);
        $droit = count($droit);

        if($droit < 3){
            $isDisabled = "disabled";
        }
        else{
            $isDisabled = "";
        }

        return view('VueSecteur.GestionSecteur',['secteur'=>$secteur, 'user'=>$user,'isDisabled'=>$isDisabled]);
    }

    public function update($id){
        $secteur = Secteur::find($id);
        $secteur->SecLibelle = Request("Libelle");
        $secteur->save();
        return redirect('Secteur/gestion/'.$id.'');
    }

    public function before_create(){
        return view('VueSecteur.NouveauSecteur');
    }

    public function create(){
        $secteur = New Secteur();
        $secteur->SecLibelle = Request("Libelle");
        $secteur->save();
        return redirect('Secteur/gestion/'.$secteur->IdSecteur.'');
    }

    public function confirmDelete($id){
        $secteur = Secteur::find($id);
        return view('VueSecteur.SuppressionSecteur',['secteur'=>$secteur]);
    }

    public function delete($id){
        // Verification contraintes
        $count = Entreprise::where("IdSecteur",$id)->get();
        $secteur = Secteur::find($id);
        if(count($count) > 0){
            return view('VueSecteur.SuppressionSecteur',['secteur'=>$secteur,'this_alert'=> true]);
        }
        else{
            $secteur->delete();
            return redirect('Secteur/all');
        }
    }
}
