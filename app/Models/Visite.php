<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visite extends Model
{
    protected $table = 'VISITE';
    protected $primaryKey = 'id';//['IdEntreprise','VisDate','IdContact']
    public $timestamps = false;


    
    public function Entreprise()
    {
      return $this->hasOne(Entreprise::class,"IdEntreprise","IdEntreprise");
    }

    public function Contact()
    {
      return $this->hasOne(Contact::class,"idContact","idContact");
    }

}



