@include('header',['title'=> 'Menu'])
<body>
   <h1>Nouvelle visite</h1>
   <form method='POST' action=''>
            
      @method('PUT')
      @csrf  

      <select class="form-select" aria-label="Default select example">
         <option selected>Open this select menu</option>
         <option value="1">One</option>
         <option value="2">Two</option>
         <option value="3">Three</option>
      </select>
   
      <input type="texte" class="form-control" name='CountryCode' >
   
      <input type="texte" class="form-control" name='District' >
   
      <input type="number" class="form-control" name='Population' >

      <button type="submit" class="btn btn-primary">Valider</button>
   </form>
</body>