@include('header',['title'=> 'Detail Entreprise'])
@include('navbar',['title'=> "Modification d'une Entreprise"])


<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Aperçu et Modification de l'entreprise @if ($user->roles()->where('LibelleRole','commercial')->exists())<a class="trash-right" href="/Entreprise/Suppression/{{$entreprise->IdEntreprise}}"><ion-icon size="large" name="trash-outline"></ion-icon></a>@endif</h5>
               <div class="card-body">
                  <form method='POST' action=''>
                     
                     @method('PUT')
                     @csrf  
                    
                     <label>Id Entreprise</label>  
                     <input type="texte" class="form-control" name='IdEntreprise' value="{{ $entreprise->IdEntreprise }}" disabled="disabled" {{$isDisabled}}>

                     <label>Nom<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Nom' value="{{ $entreprise->EntNom }}" required {{$isDisabled}}>
                     
                     <label>Ville<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Ville' value="{{ $entreprise->EntVille}}" required {{$isDisabled}}>
                     
                     <label>Adresse<span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Adresse' value="{{ $entreprise->EntAdresse }}" required {{$isDisabled}}>
                     
                     <br>
                     <label>Secteur<span class="obligatoire">*</span></label>
                     <select class="form-select" aria-label="Default select example" name='Secteur' required {{$isDisabled}}>
                        <option value="{{$entreprise->idSecteur}}" selected>{{$entreprise->Secteur->SecLibelle}}</option>
                        
                        @foreach ($secteurs as $secteur)
                        
                           @if ($secteur->IdSecteur == $entreprise->IdSecteur)
                              
                           @else
                              <option value="{{$secteur->IdSecteur}}">{{$secteur->SecLibelle}}</option>
                           @endif
                        @endforeach
                     </select>
                     <br>
                     <div class="text-center">
                        <button type="submit" class="btn btn-primary" {{$isDisabled}}>Valider</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>