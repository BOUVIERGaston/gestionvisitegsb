@include('header',['title'=> 'Nouvelle Visite'])
@include('navbar',['title'=> 'Nouvelle visite'])
<?php 
use App\Models\User;
use App\Models\Entreprise;
$user  = Auth::user(); 
?>

<body>
   <br><br>
   <div class="container-fluid">
      <div class="row justify-content-center">
         <div class="col-8 ">
            <div class="card">
               <h5 class="card-header text-center">Création d'une Visite</h5>
               <div class="card-body">
                  <form method='POST' action=''>
                     
                     @method('PUT')
                     @csrf  
                    
                     <label>Entreprise<span class="obligatoire">*</span></label>
                     <div class="input-group">
                        <select class="form-select" name='Entreprise' required>
                           @foreach ($entreprises as $entreprise)
                           <option value="{{$entreprise->IdEntreprise}}">{{$entreprise->EntNom}}</option>
                           @endforeach
                        </select>
                        <button class="btn btn-outline-success" type="button" onclick="window.location.href = '/Entreprise/new'">Créer</button>
                     </div>
                     <br>

                     <label>Contact <i>( Format : "Nom entreprise - nom prenom du contact")</i><span class="obligatoire">*</span></label>
                     <div class="input-group">
                        <select class="form-select" name='Contact' required>
                           @foreach ($contacts as $contact)
                              <?php
                                 $monEntreprise = Entreprise::find($contact->IdEntreprise);
                              ?>
                              <option value="{{$contact->IdContact}}">{{$monEntreprise->EntNom." - ".$contact->ConNom." ".$contact->ConPrenom}}</option>
                              
                           @endforeach
                        </select>
                        <button class="btn btn-outline-success" type="button" onclick="window.location.href = '/Contact/new'">Créer</button>
                     </div>
                     <br>

                     <label>Date de visite <i>( Format : "AAAA-MM-JJ")</i><span class="obligatoire">*</span></label>
                     <input type="texte" class="form-control" name='Date' placeholder="AAAA-MM-JJ">
                     <br>
                     
                     <label>Commentaire de la visite</label>
                     <textarea class="form-control" rows="5" name="Comment"></textarea>
                     <br>
                     
                     <div class="text-center">
                        <button type="submit" class="btn btn-primary">Valider</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>