@include('header',['title'=> 'Contacts'])
@include('navbar',['title'=> "Visualisation des Contacts"])


<body>

   <br>
   <div class="row text-center" id="search-form">
      <form method='POST' action=''>
                     
         @method('PUT')
         @csrf  
         <input type="text" class="form-control" name="search" id="search-input" placeholder="Rechercher par nom" 
         @if(isset($content_search)) value="{{$content_search}}"@endif>
         <button class="btn btn-outline-success" type="submit">Rechercher</button>
         @if ($user->roles()->where('LibelleRole','commercial')->exists())
         <a href="new" class="button_create_table"><button type="button" class="btn btn-secondary">Nouveau Contact</button></a>
         @endif
      </form>
      
   </div>
   <br>
   
   <div class="row justify-content-center">
      <div class="col-1"></div>
      <div class="col-10">
         <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
               <th scope="col">Id Contact</th>
               <th scope="col">Nom</th>
               <th scope="col">Prenom</th>
               <th scope="col">Poste</th>
               <th scope="col">Telephone</th>
               <th scope="col">Entreprise</th>
            </tr>
            </thead>
            <tbody>
               @foreach ($contacts as $contact)
                  <tr>
                     <td scope='row'>{{ $contact->IdContact }}</td>
                     <td scope='row'>{{ $contact->ConNom }}</td>
                     <td scope='row'>{{ $contact->ConPrenom }}</td>
                     <td scope='row'>{{ $contact->ConPoste }}</td>
                     <td scope='row'>{{ $contact->ConTel }}</td>
                     <td scope='row'><a href="/Entreprise/gestion/{{$contact->IdEntreprise}}">{{ $contact->Entreprise->EntNom }}</a></td>
                     <td scope='row'>
                        <a href="gestion/{{$contact->IdContact}}"><button  type="button" class="btn btn-success">Modifier</button></a>
                        @if ($user->roles()->where('LibelleRole','commercial')->exists())
                        <a href="Suppression/{{$contact->IdContact}}"><button type="button" class="btn btn-danger">Supprimer</button></a>
                        @endif
                     </td>
                  <tr>
               @endforeach
            </tbody>
         </table>
      </div>
      <div class="col-1"></div>
   </div>
</body>
<footer>
   @if(isset($contacts))
      <ul class="pagination justify-content-center mb-4">
         {{$contacts->links("pagination::bootstrap-4")}}
      </ul>
   @endif
</footer>