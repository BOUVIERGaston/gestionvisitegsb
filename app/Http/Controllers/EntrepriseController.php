<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Entreprise;
use App\Models\Secteur;
use App\Models\Visite;
use App\Models\Contact;

class EntrepriseController extends Controller
{
    public function show($id){
        $entreprise = Entreprise::find($id);
        $secteurs = Secteur::all();

        $user  = auth()->user(); 
        $droit = DB::select('SELECT role_IdRole FROM role_user WHERE user_id = ?', [$user->id]);
        $droit = count($droit);

        if($droit == 1){
            $isDisabled = "disabled";
        }
        else{
            $isDisabled = "";
        }

        return view('VueEntreprise.DetailEntreprise',['entreprise'=>$entreprise,'secteurs'=>$secteurs,'isDisabled'=>$isDisabled,'user'=>$user]);
    }

    public function find($id){
        $entreprise = Entreprise::find($id);
        return $entreprise;
    }

    public function paginate(){
        $entreprises = Entreprise::paginate(10);
        $user  = auth()->user();
        return view('VueEntreprise.Entreprises',['entreprises'=>$entreprises,'user'=>$user]);
    }

    public function paginate_search(){
        $content = Request("search");
        $entreprises = Entreprise::where('EntNom', 'like', '%'.$content.'%')->paginate(10);
        $user  = auth()->user();
        return view('VueEntreprise.Entreprises',['entreprises'=>$entreprises, 'content_search'=>$content,'user'=>$user]);
    }

    public function update($id){
        $entreprise = EntrepriseController::find($id);
        $entreprise->EntNom = Request("Nom");
        $entreprise->EntVille = Request("Ville");
        $entreprise->EntAdresse = Request("Adresse");
        $entreprise->IdSecteur = Request("Secteur");
        $entreprise->save();
        return redirect('Entreprise/gestion/'.$id.'');
    }

    public function before_create(){
        $secteurs = Secteur::all();
        return view('VueEntreprise.NouvelleEntreprise',['secteurs'=>$secteurs]);
    }

    public function create(){
        $entreprise = New Entreprise;
        $entreprise->EntNom = Request("Nom");
        $entreprise->EntVille = Request("Ville");
        $entreprise->EntAdresse = Request("Adresse");
        $entreprise->IdSecteur = Request("Secteur");
        $entreprise->save();
        return redirect('Entreprise/gestion/'.$entreprise->IdEntreprise.'');
    }

    public function confirmDelete($id){
        $entreprise = Entreprise::find($id);
        return view('VueEntreprise.SuppressionEntreprise',['entreprise'=>$entreprise]);
    }

    public function delete($id){
        // Verification contraintes
        $count = Visite::where("IdEntreprise",$id)->get();
        $count2 = Contact::where("IdEntreprise",$id)->get();
        $entreprise = Entreprise::find($id);
        if(count($count) > 0 | count($count2) > 0){
            return view('VueEntreprise.SuppressionEntreprise',['entreprise'=>$entreprise,'this_alert'=> true]);
        }
        else{
            $entreprise->delete();
            return redirect('Entreprise/all');
        }
    }
}
