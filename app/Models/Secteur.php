<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Secteur extends Model
{
    protected $table = 'SECTEUR';
    protected $primaryKey = 'IdSecteur';//['IdEntreprise','VisDate','IdContact']
    public $timestamps = false;

    public function Entreprise()
    {
      return $this->hasMany(Secteur::class,"IdEntreprise","IdEntreprise");
    }
}
