<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function paginate(){
        $users = User::paginate(11);
        return view('VueAdministrateur.Users',['users'=>$users]);
    }

    public function paginate_search(){
        $content = Request("search");
        $users = User::where('name', 'like', '%'.$content.'%')->paginate(11);
        return view('VueAdministrateur.Users',['users'=>$users, 'content_search'=>$content]);
    }



    // UPDATE //
    public function show($id){
        $user = User::find($id);
        $droit = DB::select('SELECT role_IdRole FROM role_user WHERE user_id = ?', [$id]);
        $droit = count($droit);
        return view('VueAdministrateur.GestionUser',['user'=>$user,'droit'=>$droit]);
    }

    public function update($id){
        $user = User::find($id);
        $droit = DB::select('SELECT role_IdRole FROM role_user WHERE user_id = ?', [$id]);
        $droit = count($droit);
        // Gestion du passwd
        if(Request("password") != "" || Request("confirm-password")!= ""){
            if(Request("password") == Request("confirm-password")){
                $user->password = Hash::make(Request("password"));
            }
            else{
                echo '
                    <title>Probleme MDP</title>
                    <link href="/css/lecss.css" rel="stylesheet" type="text/css">
                    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
                    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
                    
                    <div class="modal fade show" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterTitle" style="display: block;" aria-modal="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="exampleModalCenterTitle">Erreur avec le nouveau mot de passe</h1>
                                </div>
                                <div class="modal-body">
                                    <p>
                                    Les deux mots de passes n&rsquo;ont pas été reconnus, <br>
                                    ils sont peut être differents<br>
                                    Aucune modification n&rsquo;a été effectuée.
                                    </p>
                                </div>
                                <div class="modal-footer justify-content-center">
                                    <a href="/User/gestion/'.$id.'"><button type="button" class="btn btn-primary">D&rsquo;accord</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        ';
                    return;
                    }
        }
        // Gestion des droits
        if(Request("droit") != $droit){
            DB::delete('DELETE FROM role_user WHERE user_id = ?', [$id]);

            $idRole = Request("droit");
            if($idRole == 1){
                DB::insert('insert into role_user (user_id, role_IdRole) values (?, ?)', [$id , 1]); 
            }
            else if($idRole == 2){
                DB::insert('insert into role_user (user_id, role_IdRole) values (?, ?)', [$id , 1]);
                DB::insert('insert into role_user (user_id, role_IdRole) values (?, ?)', [$id , 2]);
            }
            else if($idRole == 3){
                DB::insert('insert into role_user (user_id, role_IdRole) values (?, ?)', [$id , 1]);
                DB::insert('insert into role_user (user_id, role_IdRole) values (?, ?)', [$id , 2]);
                DB::insert('insert into role_user (user_id, role_IdRole) values (?, ?)', [$id , 3]);
            }
        }

        $user->name = Request("name");
        $user->email = Request("email");
        $user->save();
        return redirect('/User/gestion/'.$id.'');
    }
    // END UPDATE //


    // DELETE //
    public function confirmDelete($id){
        $user = User::find($id);
        return view('VueAdministrateur.SuppressionUser',['user'=>$user]);
    }

    public function delete($id){
        $user = User::find($id);
        
        $user->delete();
        return redirect('/User/all');
    }
    // END DELETE //
}
