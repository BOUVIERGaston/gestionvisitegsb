<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Visite;
use App\Models\Entreprise;
use App\Models\User;
use App\Http\Middleware\Authenticate;

use Illuminate\Support\Facades\DB;


class VisiteController extends Controller
{
    public function showAll(){
        // $visites = Visite::all();
        $visites = Visite::orderBy('VisDate', 'asc')->get();
        return view('EspaceUtilisateur',['visites'=>$visites]);
    }

    public function show($id){
        $visite = Visite::find($id);
        $entreprises = Entreprise::orderBy('EntNom', 'asc')->get();
        $contacts = DB::select('SELECT CONTACT.* FROM CONTACT, ENTREPRISE WHERE CONTACT.IdEntreprise = ENTREPRISE.IdEntreprise ORDER BY EntNom');

        $user  = auth()->user(); 
        $droit = DB::select('SELECT role_IdRole FROM role_user WHERE user_id = ?', [$user->id]);
        $droit = count($droit);

        if($droit == 1){
            $isDisabled = "disabled";
        }
        else{
            $isDisabled = "";
        }

        return view('VueVisite.GestionVisite',['contacts'=>$contacts,'entreprises'=>$entreprises,'visite'=>$visite,'isDisabled'=>$isDisabled]);
    }

    public function update($id){
        $dateInput = Request("Date"); // Récupérer la valeur du champ d'entrée
        
        if ($this->validerFormatDate($dateInput) == false) {
            echo '
            <title>Probleme Date</title>
            <link href="/css/lecss.css" rel="stylesheet" type="text/css">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
            
            <div class="modal fade show" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterTitle" style="display: block;" aria-modal="true" role="dialog">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalCenterTitle">La visite n&rsquo;a pas pu être modifiée</h1>
                        </div>
                        <div class="modal-body">
                            <p>
                            Le format de date entrée ou la date n&rsquo;est pas valide.<br>
                            Aucune modification n&rsquo;a été effectuée.
                            </p>
                        </div>
                        <div class="modal-footer justify-content-center">
                            <a href="/Visite/gestion/'.$id.'"><button type="button" class="btn btn-primary">D&rsquo;accord</button></a>
                        </div>
                    </div>
                </div>
            </div>
                ';
        } else {
            $visite = Visite::find($id);
            $visite->IdContact = Request("Contact");
            $visite->IdEntreprise = Request("Entreprise");
            $visite->VisDate = $dateInput;
            $visite->VisCommentaire = Request("Comment");
            $visite->save();
            return redirect('/Visite/gestion/'.$id.'');
        }
    }

    // CREATE //
    public function before_create(){
        $entreprises = Entreprise::orderBy('EntNom', 'asc')->get();
        $contacts = DB::select('SELECT CONTACT.* FROM CONTACT, ENTREPRISE WHERE CONTACT.IdEntreprise = ENTREPRISE.IdEntreprise ORDER BY EntNom');
        return view('VueVisite.NouvelleVisite',['contacts'=>$contacts,'entreprises'=>$entreprises]);
    }

    public function create(){
        $dateInput = Request("Date"); // Récupérer la valeur du champ d'entrée
        
        if ($this->validerFormatDate($dateInput) == false) {
            echo '
            <title>Probleme Date</title>
            <link href="/css/lecss.css" rel="stylesheet" type="text/css">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
            
            <div class="modal fade show" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterTitle" style="display: block;" aria-modal="true" role="dialog">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalCenterTitle">La visite n&rsquo;a pas pu être crée</h1>
                        </div>
                        <div class="modal-body">
                            <p>
                            Le format de date entrée ou la date n&rsquo;est pas valide.<br>
                            La visite n&rsquo;a ainsi pas été crée.
                            </p>
                        </div>
                        <div class="modal-footer justify-content-center">
                            <a href="/Visite/new"><button type="button" class="btn btn-primary">D&rsquo;accord</button></a>
                        </div>
                    </div>
                </div>
            </div>
                ';
        } else {
        $visite = New Visite();
        $visite->IdContact = Request("Contact");
        $visite->IdEntreprise = Request("Entreprise");
        $visite->VisDate = Request("Date");
        $visite->VisCommentaire = Request("Comment");
        $visite->save();
        return redirect('Visite/gestion/'.$visite->id.'');
        }
    }
    // END CREATE //

    // DELETE //
    public function confirmDelete($id){
        $visite = Visite::find($id);
        return view('VueVisite.SuppressionVisite',['visite'=>$visite]);
    }

    public function delete($id){
        $visite = Visite::find($id);
        
        $visite->delete();
        return redirect('/home');
    }
    // END DELETE //



    ///////////////////////
    // fONCTIONS INTERNES
    ///////////////////////

    private function validerFormatDate($dateTexte) {
        if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $dateTexte)) {
            return false;
        }
    
        $format = 'Y-m-d';
        $date = \DateTime::createFromFormat($format, $dateTexte);
        return $date && $date->format($format) === $dateTexte;
    }
}
